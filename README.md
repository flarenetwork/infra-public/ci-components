# ci-components

A [gitlab components](https://docs.gitlab.com/ee/ci/components/#components-repository) repo with multiple CI components (mainly linters) maintained for ([@flarenetwork](https://gitlab.com/flarenetwork/)) to make my (and maybe also your) life simpler.


For usage examples look at `./templates/<component-name>/README.md`.

