# 🌍 Lint-Terraform

Lint terraform files using [`terraform fmt`](https://developer.hashicorp.com/terraform/cli/commands/fmt) utility.

## Example usage

```yaml
include:
- component: gitlab.com/flarenetwork/infra-public/ci-components/lint-terraform@1.0.0 
  # Example input overwrite
  inputs:
    allow_failure: true
```
