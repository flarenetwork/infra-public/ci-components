# 📜 Lint-Helm

Lint all helm charts using [`helm lint`](https://helm.sh/docs/helm/helm_lint/)

## Example usage

```yaml
include:
- component: gitlab.com/flarenetwork/infra-public/ci-components/lint-helm@1.0.0 
```
