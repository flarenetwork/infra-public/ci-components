# 🐳 Lint-Dockerfile

Dockerfile linter using [Hadolint](https://github.com/hadolint/hadolint) utility.

Searches for files ending with `dockerfile`


## Example usage

```yaml
include:
- component: gitlab.com/flarenetwork/infra-public/ci-components/lint-dockerfile@1.0.0 
```
