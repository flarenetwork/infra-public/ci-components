# 🐚 Lint-Shell

Lint shell files using [Shellcheck](https://github.com/koalaman/shellcheck) utility

## Example usage

```yaml
include:
- component: gitlab.com/flarenetwork/infra-public/ci-components/lint-shell@1.0.0 
```
