mkdocs==1.5.3
mkdocs-minify-plugin==0.7.1
mkdocs-redirects==1.2.0
mkdocs-material==9.4.14
mkdocs-same-dir==0.1.2
mkdocs-git-revision-date-localized-plugin==1.2.1
pymdown-extensions>=10.5
Pygments>=2.16.1
